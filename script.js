"use strict" ;

// 1. Опишіть своїми словами що таке Document Object Model (DOM)

// DOM - дозволяє JavaScript маніпулювати та працювати з елементами та стилями веб-сайту.
// DOM- представляє структуру веб-сайту так, щоб комп'ютери могли її розуміти. 

// 2. Яка різниця між властивостями HTML-елементів innerHTML та innerText?

// innerHTML та innerText - це властивості, які дозволяють отримувати або встановлювати текстовий вміст елемента в HTML. 
// innerHTML дозволяє отримувати або встановлювати код, який міститься в середині елемента. innerHTML дозволяє  вставляти HTML теги та структуру вмісту.

// innerText дозволяє встановлювати текстовий вміст елемента, виключаючи теги. Завдяки innerText всі HTML теги вони відображаються як звичайний текст.

// 3. Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?

// querySelector , getElementById  , querySelectorAll , getElementById
// getElementById є найшвидшим оскільки використовується для отримання елемента за його ідентифікатором,a ідентифікатор повинен бути унікальним на сторінці.

// 4. Яка різниця між nodeList та HTMLCollection?

// NodeList - це список  вузлів (теги, коментарі, текстові ноди)
// HTMLCollection - це всіх елементів або тегів
// Якщо потрібно отримати колекцію елементів, HTMLCollection буде зручніший.
// Якщо потрібно працювати з вузлами DOM, то NodeList буде кориснішим.

// 1. Знайдіть всі елементи з класом "feature", запишіть в змінну, вивести в консоль. Використайте 2 способи для пошуку елементів.

const elementsFeature = document.querySelectorAll(".feature");
console.log("elementsFeature:", elementsFeature);

for (const element of elementsFeature) {
  element.style.textAlign = "center";
}
//-------------------------------------------------------------------------/  
const elementsFeature2 = document.getElementsByClassName("feature");
console.log("elementsFeature2:", elementsFeature2);

for (const element of elementsFeature2) {
  element.style.textAlign = "center";
}

// 2. Змініть текст усіх елементів h2 на "Awesome feature".

const h2Element = document.querySelectorAll("h2");

for (const h2 of h2Element) {
  h2.textContent = "Awesome feature";
}

// 3. Знайдіть всі елементи з класом "feature-title" та додайте в кінець тексту елементу знак оклику "!".

const featureTitle = document.querySelectorAll(".feature-title");

for (const title of featureTitle) {
  title.textContent += "!";
}